var gulp = require('gulp'),
  sass = require('gulp-sass'),
  browserSync = require('browser-sync').create(),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  gutil = require('gulp-util'),
  plumber = require('gulp-plumber'),
  imagemin = require('gulp-imagemin'),
  runSequence = require('run-sequence'),
  sourcemaps = require('gulp-sourcemaps');


// Start browserSync server
gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
    cors: true
  });
});

// Create dist structure
gulp.task('directories', function () {
  return gulp.src('*.*', {read: false})
    .pipe(gulp.dest('./app/dist/img'));
});

// Compile sass into one css file
gulp.task('sass', function () {
  return gulp.src('app/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({ outputStyle: 'compressed' })) // Using gulp-sass
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('app/dist/css'))
    //get our sources via sourceMaps
    .pipe(browserSync.reload({
      stream: true
    }));
});

// compress css for deployment 
gulp.task('sass-deploy', function () {
  return gulp.src('app/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({ outputStyle: 'compressed' })) // Using gulp-sass
    .pipe(gulp.dest('app/dist/css'))
    //get our sources via sourceMaps
    .pipe(browserSync.reload({
      stream: true
    }));
});

//compiling our Javascripts
gulp.task('scripts', function () {
  //this is where our dev JS scripts are
  return gulp.src(['app/js/*.js'])
  //prevent pipe breaking caused by errors from gulp plugins
    .pipe(plumber())
    //this is the filename of the compressed version of our JS
    .pipe(concat('jqueries.js'))
    // Compress js after adding all the files together
    .pipe(uglify())
    //catch errors
    .on('error', gutil.log)
    //where we will store our finalized, compressed script
    .pipe(gulp.dest('app/dist/js'))
    //notify browserSync to refresh
    .pipe(browserSync.reload({ stream: true }));
});

// Optimize images
gulp.task('images', function () {
  gulp.src(['app/img/**/*.jpg', 'app/img/**/*.png', 'app/img/**/*.svg', 'app/img/**/*.ico'])
  //prevent pipe breaking caused by errors from gulp plugins
    .pipe(plumber())
    .pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
    .pipe(gulp.dest('app/dist/img/'));
});

// Watchers
gulp.task('watch', function () {
  gulp.watch('app/sass/**/*.scss', ['sass'], browserSync.reload);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/img/**/*.jpg', ['images'], browserSync.reload);
  gulp.watch('app/img/**/*.png', ['images'], browserSync.reload);
  gulp.watch('app/img/**/*.svg', ['images'], browserSync.reload);
  gulp.watch('app/img/**/*.ico', ['images'], browserSync.reload);
  gulp.watch('app/js/*.js', ['scripts'], browserSync.reload);
});

// Default
gulp.task('default', function (callback) {
  runSequence(['sass', 'directories', 'scripts', 'browserSync', 'images'], 'watch',
    callback
  );
});

// Build
gulp.task('deploy', function (callback) {
  runSequence(['sass-deploy', 'directories', 'scripts', 'images'],
    callback
  );
});

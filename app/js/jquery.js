// Cookie Policy

function cookiePolicy() {
    var cookiePolicyContent = '<div id="wrapper--cookie" class="wrapper  wrapper--cookie" data-state="active">' +
	                               '<div class="layout-wrapper">' +
		                              '<div class="layout  layout--middle">' +
			                             '<div class="layout__item  u-1/1">' +
				                            '<p class="u-m0">This website works best using cookies. If you continue to use this site, we\'ll assume that you are happy to receive cookies. <a href="/terms-and-conditions">Terms of Use</a>.</p>' +
				                                '<a href="#" class="closeButton"><i class="far fa-times-circle"></i></a>' +
			                                 '</div>' +
		                                  '</div>' +
	                                   '</div>' +
                                '</div>';
    $('body').prepend(cookiePolicyContent);
    
    var cookie = $('#wrapper--cookie');
	if ( $.cookie('cookie_accepted')) {
		cookie.attr('data-state', 'inactive');
        console.log('I have a cookie yummy');
	} else {
		cookie.attr('data-state', 'active');
        console.log('I have no a cookie!');
	}

	$('.closeButton').on('click', function(e) {
        e.preventDefault();
		cookie.attr('data-state', 'inactive');
		$.cookie('cookie_accepted', '1', { expires: 365, path:'/' });
	});

    $('.closeButtonPR').on('click', function(e) {
       
        $('#prSection').hide();
    });


}

    
// Function to fix the bottom part of the nav on scroll 

function fixedNav() {
    if($(window).width() > 1024) {
        var stickySidebar = $('.stickyHeader').offset().top;
        var headerHeight = $('.header').outerHeight();
        $(window).scroll(function() {  
            if ($(window).scrollTop() > stickySidebar) {
                $('.header').addClass('fixed');
                $('body').css({'margin-top': headerHeight});
            }
            else {
                $('.header').removeClass('fixed');
                $('body').css({'margin-top': '0px'});
            }  
        });
    } else {
        
    }
}


// Home hero slider 


function heroSlider(){
    $('.heroSlider').slick({
     dots: true,
     infinite: true,
     fade: true,
     arrows: false,
     autoplay: true,
     speed: 1000,
     responsive: true
   });
}

// New Parallax Scrolling backgrounds

function parallaxBG() {
    if($(window).width() > 1024) {
        $.stellar({
            horizontalScrolling: false,
            verticalOffset: 0
        });
    } else {
        
    }
}


// Content Carousel
function carousel() {
   $('.logoCarousel').slick({
     dots: false,
     infinite: true,
     speed: 300,
     slidesToShow: 6,
     slidesToScroll: 6,
     arrows: false,
     variableWidth: true,
     autoplay: true,
     speed: 1000,
     responsive: [
       {
         breakpoint: 768,
         settings: {
           slidesToShow: 4,
           slidesToScroll: 4
         }
       },
       {
         breakpoint: 480,
         settings: {
           slidesToShow: 1,
           slidesToScroll: 1
         }
       }
       
     ]
   });
      
    
} 

function hamburgerAnimation() {
    $('#nav-icon').click(function(){
        $(this).toggleClass('open');
        $('.header .navContainer .nav').slideToggle('open');
    });
}

function parallaxImages() {
    $(window).trigger('resize').trigger('scroll');
}

function animatedBlocks() {
     function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return (elemTop <= docViewBottom);
    }
    
    $(window).scroll(function () {
        $('.animatedBlocks, .servicesBlocks .servicesBlocksRow').each(function () {
            if (isScrolledIntoView(this) === true) {
                $(this).addClass('inview');
            } else {
                $(this).removeClass('inview');
            }
        });

    });
}

function equalHeight() {
	
	 $('.row').each(function(){  
	 // Cache the highest
      var highestBox = 0;
		// Select and loop the elements you want to equalise
      $('.heroBlock', this).each(function(){        
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }      
      });        
      $('.heroBlock',this).height(highestBox);
      
	   var highestBox = 0;
		// Select and loop the elements you want to equalise
      $('.herosubBlock', this).each(function(){        
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }      
      });        
      $('.herosubBlock',this).height(highestBox);


        var highestBox = 0;
        // Select and loop the elements you want to equalise
      $('.serviceText', this).each(function(){        
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }      
      });        
      $('.serviceText',this).height(highestBox);





    }); 	







}
// Sort the articles
function sortArticles() {

    $('.sortBy').change(function() {
        $('#loadMore').fadeIn('slow'); 
        var filterValue = $(this).val();      
        var row = $('.insightBox'); 
        if (filterValue === 'All') {
             $('.insightBox').removeClass("insightBox--hidden");  
             $('.insightBox').addClass("insightBox--hidden");     
             $('.insightBox').slice(0,6).removeClass("insightBox--hidden");   
        } else { 
             j = 0;  
            row.each(function(i, el) {
                 $(el).addClass("insightBox--hidden");                      
                 if($(el).attr('data-type') == filterValue) {
                        if ( j < 6) {
                            $(el).removeClass("insightBox--hidden");   
                            j = j+1;
                         }
                }
            });
                if (j < 6){                         
                    $('#loadMore').fadeOut('slow'); 
                } 
       }
    });
}

function loadMore() {
    // show first six items
    $('.insightBox').slice(6).addClass("insightBox--hidden");
    if ($('.insightBox').length <= 6) {
        $('#loadMore').hide();
    } else {
       $('#loadMore').show(); 
    }
    $('#loadMore').on('click', function (e) {
        e.preventDefault();
         var item = $('.insightBox');
        var filterValue = $('.sortBy').val();   
        //count number of items
            if (filterValue == "All"){
                i = 0;  
                item.each(function() {

                     if ($(this).hasClass("insightBox--hidden")){
                        if ( i < 3) {
                            $(this).removeClass("insightBox--hidden");   
                            i = i+1;
                         } else {

                            return false;
                         }
                                  
                    }
                });  

                if ($('.insightBox--hidden').length == 0) {           
                    $('#loadMore').fadeOut('slow');
                }

             } else {
                 j = 0; 
                item.each(function() {
                   

                    if($(this).attr('data-type') == filterValue) {
                        if ( j < 3) {

                            if ($(this).hasClass("insightBox--hidden")){
                               $(this).removeClass("insightBox--hidden");   
                                j = j+1;
                             }   
                         } 
                    }  
                 });                   
                if (j < 3){                         
                    $('#loadMore').fadeOut('slow'); 
                } 
             }   

    });

}

function mobileNavigation() {
    $('.header').append('<div class="mobileNav"><ul class="remove-bottom"></ul></div>');
    var topNav = $('.topNav ul').html();
    var mainNav = $('.mainNav ul').html();
    $('.mobileNav ul').append(mainNav, topNav);
    $('#nav-icon').click(function(){
        $(this).toggleClass('open');
        $('.mobileNav').slideToggle();
    });

}

function casestudyScroll() {
	
	$('.csLink').click(function(e){
		 e.preventDefault();
	     var block = $(this).attr('data-block');
	    //console.log(block);
	
		 document.getElementById(block).scrollIntoView({ behavior: 'smooth'});
	});
	
}

// Smooth scroll 

function smoothScroll() {
    $('.internalLink').click(function(event){
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        event.preventDefault();
    });
}
  

// Vacancy filler


function mapPopup() {
    $('.mapOverlay').each(function(){
        $(this).prepend('<div class="closeButton"><i class="fas fa-times"></i></div>');
    });
	
	$('.locationPopup').on('click', function(event) {
		
		var long = $(this).data("long");
		var lat = $(this).data("lat");
		
		//var myLatLng = {lat: lat, lng: long};
        
		map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: lat, lng: long},
          zoom: 15,
		  disableDefaultUI: true
        });
     //   marker = new google.maps.Marker({
     //     position: {lat: lat, lng: long},
     //     map: map,
     //     title: 'Hello World!'
     //   });

    var location = $(this).data("location");

    for( var i = 0, len = offices.length; i < len; i++ ) {
    if( offices[i][0] == location ) {
        var address = offices[i][1][1];
        var country = offices[i][1][2];

        $('.locCity').text(offices[i][1][0]);
        $('.locCountry').text(country);
        $('.locTelephone').text(offices[i][1][4]);
        $('.locAddress').html(address);
        //mailto
        $('.mapEmail').attr('href', 'mailto:'+offices[i][1][3]);
          //directions

          var directionsURL = "https://www.google.com/maps/dir/?api=1&destination=";
          var newA = address.replace(/<br \/>/g, "+");
          $('.mapDirections').attr('href', directionsURL + newA + "+" + country);

    marker = new google.maps.Marker({
         position: {lat: lat, lng: long},
         map: map,
         title: offices[i][1][0]
       });

        break;
      }
    }


		$('body').addClass('popupOpen');
		$('#locationMap').css('top', $(document).scrollTopop + 'px');
		$('#locationMap').css ('left','0');
	});	
	
	$('.mapOverlay .closeButton').on('click', function(event) {
        $('body').removeClass('popupOpen');
		event.preventDefault();
			$(this).parent().css('top', '0');
			$(this).parent().css ('left','-100%');
	});	
	
	$('.mapDirections').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		
		 var addressValue = $(this).attr("href");
        var win = window.open(addressValue, '_blank');
		win.focus;
	});		
	
	$('.mapEmail').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		
		 var addressValue = $(this).attr("href");
         window.location = addressValue ;

	});		
	
}

 var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 51.2665, lng: -1.0924},
          zoom: 15,
		  disableDefaultUI: true
        });
      }

function modalPopup() {
	$('.csOverlay').each(function(){
        $(this).prepend('<div class="closeButton"><i class="fas fa-times"></i></div>');
    });
    
	$('.CSpopup').on('click', function(event) {
		event.preventDefault();
		
		var caseStudy = $(this).data("case");
		
		/*
		var map;
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
	*/
		$('body').addClass('popupOpen');
		$('#'+caseStudy).css('top', $(document).scrollTopop + 'px');
		$('#'+caseStudy).css ('left','0');
	});
    
    // Merlin click to show case study 
    
    $('.CSpopupButton').on('click', function(event) {
        //changeBG();
		event.preventDefault();
		
		var caseStudy = $(this).attr("href");
        console.log(caseStudy);
		
		/*
		var map;
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
	*/
		$('body').addClass('popupOpen');
		$(caseStudy).css('top', $(document).scrollTopop + 'px');
		$(caseStudy).css ('left','0');
	});
	
	$('.closeButton').on('click', function(event) {
		event.preventDefault();
			$(this).parent().css('top', '0');
			$(this).parent().css ('left','-100vw');
            $('body').removeClass('popupOpen');
	});	
	
	$('.accordian').on('click', function(event) {
		event.preventDefault();
		
		var caseStudy = $(this).data("case");
		
		if ( $('#'+caseStudy).is( ":hidden" ) ) {
			$('#'+caseStudy).slideDown( "slow" );
		} else {
			$('#'+caseStudy).slideUp( "slow"  );
		}
	});	
	
	$('.csAccordian').on('click', function(event) {
		event.preventDefault();

		var caseStudy = $(this).attr('id');
		
			$('#'+caseStudy).slideUp( );
	});	
	
	
}


function changeBG() {
    var bgColorArray = [
        'dist/img/modalSealifeBGOne.jpg',
        'dist/img/travelBG.jpg'
    ],
    selectBG = bgColorArray[Math.floor(Math.random() * bgColorArray.length)];

    $('#sealifeGuidebooks .heroImage').css('background-image', 'url(' + selectBG + ')');
}





function sizeOfMap(){
    $('.map').each(function(){
        var mapWidth = $(this).parent().width();
        var mapHeight = $(this).parent().parent().height()
        $(this).width(mapWidth);
        $(this).height(mapHeight);
    });
}

function backToTop() {
    $('body').append('<div class="backToTopButton"><i class="fas fa-angle-up"></i></div>');
    $('.backToTopButton').hide();
    $('.backToTopButton').click(function() {
        $('html, body').animate({scrollTop: 0}, 1000);
     });
    $(window).scroll(function(){
      if($(window).scrollTop() > 400){
          $('.backToTopButton').fadeIn();
      } else {
          $('.backToTopButton').fadeOut();
      }
    });
}

function smoothScrollExternal() {
    var urlHash = window.location.href.split("#")[1];
    var headerHeight = $('.stickyHeader').outerHeight();
    console.log(urlHash);
    $('html,body').animate({
        scrollTop: $('#' + urlHash).offset().top - headerHeight
    }, 1000);
}


$(document).ready(function(){
    cookiePolicy()
    backToTop()
    fixedNav()
    mobileNavigation()
	equalHeight()
    parallaxBG()
	smoothScroll()
    animatedBlocks()
    carousel()
    parallaxImages()
    loadMore()
	casestudyScroll()
	modalPopup()
    sortArticles()
	mapPopup()
	sizeOfMap()
})

$(window).load(function (){
    //smoothScrollExternal()
});

   



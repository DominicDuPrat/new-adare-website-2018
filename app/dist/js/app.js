 

  var width = 1050,
  height = 650,
  sens = 0.35,
  focused;

  /*
//Setup path for outerspace
        var space = d3.geo.azimuthal()
            .mode("equidistant")
            .translate([width / 2, height / 2]);

        space.scale(space.scale() * 3);

        var spacePath = d3.geo.path()
            .projection(space)
            .pointRadius(1);
*/

  //Setting projection

  var projection = d3.geo.orthographic()
  .scale(500)
  .rotate([0, 0])
  .translate([width / 2, height / 2])
  .clipAngle(90);

  var path = d3.geo.path()
  .projection(projection);

  //SVG container

  var svg = d3.select("#earth").append("svg")
  .attr("width", width)
  .attr("height", height)
  .attr("viewBox","0 0 1000 500")
  .attr("preserveAspectRatio","xMaxYMin meet");

   var ocean_fill = svg.append("defs").append("radialGradient")
          .attr("id", "ocean_fill")
          .attr("cx", "50%")
          .attr("cy", "50%");
      ocean_fill.append("stop").attr("offset", "10%").attr("stop-color", "#0A4F3");
      ocean_fill.append("stop").attr("offset", "100%").attr("stop-color", "#182B3A");
  
   var earthglow = svg.append("defs").append("filter").attr("id", "earth-glow");   
    earthglow.append("feColorMatrix").attr("type","matrix").attr("values","0 0 0 0 0 0 0 0 0.5 0 0 0 0 0.9 0 0 0 0 1 0"); 
earthglow.append("feGaussianBlur").attr("result","coloredBlur").attr("stdDeviation","15");  
   var emerge = earthglow.append("feMerge");
   emerge.append("feMergeNode").attr("in","coloredBlur");
   emerge.append("feMergeNode").attr("in","SourceGraphic");
  
  
      var glow = svg.append("defs").append("filter").attr("id", "glow");   
    glow.append("feColorMatrix").attr("type","matrix").attr("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 0.8 0");    
   glow.append("feGaussianBlur").attr("result","coloredBlur").attr("stdDeviation","5");   
   var merge = glow.append("feMerge");
   merge.append("feMergeNode").attr("in","coloredBlur");
   merge.append("feMergeNode").attr("in","SourceGraphic");
  
  //Adding water

  svg.append("path")
  .datum({type: "Sphere"})
  .attr("class", "water")
  .style("fill", "url(#ocean_fill)")
  .style("filter", "url(#earth-glow")
  .attr("d", path)
  .call(d3.behavior.drag()
  .origin(function() { var r = projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
  .on("drag", function() {
    var rotate = projection.rotate();
    projection.rotate([d3.event.x * sens, -d3.event.y * sens, rotate[2]]);
    svg.selectAll("path.land").attr("d", path);
    //svg.selectAll("path.cland").attr("d", path);
    svg.selectAll(".focused").classed("focused", focused = false);
  }));

//d3.select("body").append("div").attr("class", "header");


  var countryTooltip = d3.select("#earth").append("div").attr("class", "countryTooltip");
  var countryTitle = d3.select("#title").append("h2").attr("class","countrySelect");
  countryTitle.text("Find a location near you");
  var countryList = d3.select("#title").append("select").attr("id", "countries");

	var countryPopup = d3.select("#title").append("div").attr("id","countryPopup").attr("class", "cPopup");	
//	var popupImage = d3.select("#countryPopup").append("img").attr("class","popupImage").attr("id","Img");
//	popupImage.attr("src","images/china.svg");
//	popupImage.attr("width","60");
//	popupImage.attr("heighr","40");
	
	var popupClose = d3.select("#countryPopup").append("div").attr("id","close").attr("class","popupClose");
	var Close = d3.select(".popupClose").html('<i class="fas fa-times"></i>');

  var popupMain =  d3.select("#countryPopup").append("div").attr("id","popupMain").attr("class","popupFull");
  var popupLeft =  d3.select("#popupMain").append("div").attr("id","popupLeft").attr("class","popupLeft");
  var popupTitle = d3.select("#popupLeft").append("h3").attr("class","popupTitle");
  popupTitle.text("China");	
  var popupRight =  d3.select("#popupMain").append("div").attr("id","popupRight").attr("class","popupRight");
  //var popupRightData =  d3.select("#popupLeft").append("div").attr("id","popupRightData").attr("class","facts");
  var popupLeftText =  d3.select("#popupRight").append("p").attr("class","countryFact");
  popupLeftText.text("The three gorges dam is so big that created a resevoir the size of the Kingdom of Bahrain, holding back 39.3 cubic kilometers of water and has slowed the Earth's rotation by 0.06 microseconds every day.");
  //var popupRightF1 =  d3.select("#popupRightData").append("div").attr("class","fact");
  //var popupRightD1 =  d3.select("#popupRightData").append("div").attr("class","data").attr("id","data1");
  //var popupRightF2 =  d3.select("#popupRightData").append("div").attr("class","fact");
  //var popupRightD2 =  d3.select("#popupRightData").append("div").attr("class","data").attr("id","data2");
  //var popupRightF3 =  d3.select("#popupRightData").append("div").attr("class","fact");
  //var popupRightD3 =  d3.select("#popupRightData").append("div").attr("class","data").attr("id","data3");
  //var popupRightF4 =  d3.select("#popupRightData").append("div").attr("class","fact");
  //var popupRightD4 =  d3.select("#popupRightData").append("div").attr("class","data").attr("id","data4");

var popupLeftButton = d3.select("#popupLeft").append("a").attr("class","internalLink");
var popupRightButton =  d3.select("#popupLeft").append("a").attr("class","button");
var getDirections = d3.select("#popupRight").append("a").attr("class","internalLink");
$('#popupRight .internalLink').attr('href', 'https://www.google.com/maps/');

var g = svg.append("g");

getDirections.text('Get Directions');
popupRightButton.text("EMAIL US NOW");
//popupLeftButton.text('Test to see if it works');

  queue()
  .defer(d3.json, "world.json")
  .defer(d3.tsv, "selected.tsv")
  .defer(d3.json, "adarecities.geojson")
  .await(ready);

  //Main function

  function ready(error, world, countryData, cities) {
	    
    var countryById = {},randomById = {},fact1ById = {},fact2ById = {},fact3ById = {},fact4ById = {},
    countries = topojson.feature(world, world.objects.countries).features;

   rotateDD(826);

    // Drawing transparent circle markers for cities
         g.selectAll("path.cities").data(cities.features)
            .enter().append("path")
            .attr("class", "cities")
            .attr("d", path)
            .attr("fill", "#ffba00")
            .attr("fill-opacity", 0.3);
   
	var select = document.getElementById("countries");
	select.onchange = function(){
		var selected = select.options[select.selectedIndex].value;
	    rotateDD(selected);
	}

    var world = svg.selectAll("path.land")    
    .data(countries)
    .enter().append("path")
    .attr("class", "land")
    .attr("d", path)
    //.style("fill", "url(#country_fill)")
    .on("click", rotateZoom)
    //Drag event

    .call(d3.behavior.drag()
      .origin(function() { var r = projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
      .on("drag", function() {
        var rotate = projection.rotate();
        projection.rotate([d3.event.x * sens, -d3.event.y * sens, rotate[2]]);
        svg.selectAll("path.land").attr("d", path);
        svg.selectAll(".focused").classed("focused", focused = false);
      }))

    //Mouse events
     .on("click", function(d){rotateZoom(d);})

     
    .on("mouseover", function(d) {
	//$("#countryPopup").hide();
      countryTooltip.text(countryById[d.id])
     // .style("left", (d3.event.pageX - 40) + "px")
      .style("top", (d3.event.pageY - 40) + "px")
      .style("display", "block")
      .style("opacity", 1);
    })
    .on("mouseout", function(d) {

      countryTooltip.style("opacity", 0)
      .style("display", "none");
    })
    .on("mousemove", function(d) {
      countryTooltip.style("left", (d3.event.pageX-650) + "px")
      .style("top", (d3.event.pageY - 40) + "px");
    });

	 option = countryList.append("option");	
	 option.text("SELECT A DESTINATION");
     option.property("value", "0");
         //Adding countries to select
    countryData.forEach(function(d) {
    countryById[d.id] = d.name;
      switch(d.id){
        case "826":
        case "156":
        case "578":
        case "246":
        case "410":
        case "208":
        case "276":
        case "840":
        case "124":
        case "392":
        case "752":
        case "250":
        case "724":
        case "643":
        case "76":
        case "36":
        case "376":
        case "458":
        case "380":
        case "372":
        case "352":
        case "616":
		
		    
      randomById[d.id] = d.random;
      fact1ById[d.id] = d.fact1;
      fact2ById[d.id] = d.fact2;
      fact3ById[d.id] = d.fact3;
      fact4ById[d.id] = d.fact4;
		
          option = countryList.append("option");
           option.text(d.name);
          option.property("value", d.id);
          //world.attr("class","land selected")
          break;
      }      
    });
	$('select').each(function () {

    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    // Hides the select element
    $this.addClass('s-hidden');

    // Wrap the select element in a div
    $this.wrap('<div class="select"></div>');

    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect" id ="ss"></div>');

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
    $styledSelect.text($this.children('option').eq(0).text());

    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);

    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
        
    $styledSelect.click(function (e) {
        e.stopPropagation();
        $('div.styledSelect.active').each(function () {
            $(this).removeClass('active').next('ul.options').hide();
        });
        $(this).toggleClass('active').next('ul.options').toggle();
    });

    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option
    $listItems.click(function (e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        // alert($this.val());// Uncomment this for demonstration! 
		 
		 if ($("#earth").is(":hidden")){
			 
			 var code = $this.val();
			 $(".popupTitle").html(countryById[code]);
			 $(".countryFact").html(randomById[code]);
			 $("#data1").html(fact1ById[code]);
			 $("#data2").html(fact2ById[code]);
			 $("#data3").html(fact3ById[code]);
			 $("#data4").html(fact4ById[code]);
			 $("#countryPopup").show();
		 } else {
		 
		// var element = document.getElementById('countries');
		//element.value = $this.val();
		  rotateNow($this.val());		  
		 }
    });

    // Hides the unordered list when clicking outside of it
    $(document).click(function () {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

    //Country focus on option select

 var rotateMe =  function(d) {

   // Clicked on feature:
  var p = d3.geo.centroid(d);
  
  // Store the current rotation and scale:
  var currentRotate = projection.rotate();
  var currentScale = projection.scale();
  
  // Calculate the future bounding box after applying a rotation:
  projection.rotate([-p[0], -p[1]]);
  path.projection(projection);
  
  // calculate the scale and translate required:
  var b = path.bounds(d);
  var nextScale = currentScale * 1 / Math.max((b[1][0] - b[0][0]) / (width/2), (b[1][1] - b[0][1]) / (height/2));
  var nextRotate = projection.rotate();
  // Update the map:
  d3.selectAll("path")
   .transition()
   .duration(1000)
   .attrTween("d", function(d) {
       var s = d3.interpolate(currentScale, nextScale);
        return function(t) {
          projection
            .scale( currentScale > nextScale ? s(Math.pow(t,0.1)) : s(Math.pow(t,3)) );
          path.projection(projection);
          return path(d);
        }
   })
   .duration(1000)
   .attrTween("d", function(d) {
      var r = d3.interpolate(currentRotate, nextRotate);
        return function(t) {
          projection
            .rotate(r(Math.pow(t,0.33)))
          path.projection(projection);
          return path(d);
        }
   })
   .duration(1000)
   .each("end", function() {

    //console.log("This is producing an error ?"); 

  })
   ;
 }

var rotateZoom = function(d) {

	switch(d.id){
        case 826:
        case 156:
        case 578:
        case 246:
        case 410:
        case 208:
        case 276:
        case 840:
        case 124:
        case 392:
        case 752:
        case 250:
        case 724:
        case 643:
        case 76:
        case 36:
        case 376:
        case 458:
        case 380:
        case 372:
        case 352:
        case 616:
		var cId = countryById[d.id].toLowerCase().replace(/ /g,"_");		
		$("#Img").attr('src',"images/" + cId + ".svg")
		
		$(".popupTitle").html(countryById[d.id]);
        $(".countryFact").html(randomById[d.id]);
        $("#data1").html(fact1ById[d.id]);
        $("#data2").html(fact2ById[d.id]);
//        $("#data3").html(fact3ById[d.id]);
//        $("#data4").html(fact4ById[d.id]);
		
			zoomOut(d);
			rotate(d);
			zoomIn(d);
	}
}

function rotateNow(code){

if (code != 0) {

		var cId = countryById[code].toLowerCase().replace(/ /g,"_");		
		$("#Img").attr('src',"images/" + cId + ".svg")

        $(".popupTitle").html(countryById[code]);
        $(".countryFact").html(randomById[code]);
        //$("#data1").html(fact1ById[code]);
        $('#popupLeft .internalLink').html(fact1ById[code]);
        $('#popupLeft .internalLink').attr('href', 'tel:' + fact1ById[code] + '');
        //$("#data2").html(fact2ById[code]);
//        $("#data3").html(fact3ById[code]);
//        $("#data4").html(fact4ById[code]);
//        $('#popupRight .internalLink').attr('href', fact3ById[code]);
        $('.button').attr('href', 'mailto:' + fact2ById[code] + '');
			zoomOut();
			rotateDD(code);
			zoomIn();
	}
}

  function rotateDD(code){
       var rotate = projection.rotate(),
      focusedCountry = country(countries, code),
      p = d3.geo.centroid(focusedCountry);
      //console.log(focusedCountry, "hello")
      svg.selectAll(".focused").classed("focused", focused = false);

    //Globe rotating

    (function transition() {
      d3.transition()
      .duration(2500)
      .tween("rotate", function() {
        var r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
        return function(t) {
        //  projection.scale(z1(t)).rotate(r(t)).scale(z2(t));
          projection.rotate(r(t));

          svg.selectAll(".globe path").attr("d", path)
          .classed("focused", function(d, i) { return d.id == focusedCountry.id ? focused = d : false; });
        };
      })
      //.each("end", function() { console.log("This is producing an error 44"); })
      })();


  }

  function rotate(d){
       var rotate = projection.rotate(),
      focusedCountry = country(countries, d.id),
      p = d3.geo.centroid(focusedCountry);
      //console.log(focusedCountry, "hello")
      svg.selectAll(".focused").classed("focused", focused = false);

    //Globe rotating

    (function transition() {
      d3.transition()
      .duration(1000)
      .tween("rotate", function() {
        var r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
        return function(t) {
          projection.rotate(r(t));
          svg.selectAll(".globe path").attr("d", path)
          .classed("focused", function(d, i) { return d.id == focusedCountry.id ? focused = d : false; });
        };
      })
      .delay(1000)
      })();


  }

function zoomIn(d){

var currentScale = 400;
//console.log(countryById[d.id]);
// var b = path.bounds(d);
 // var nextScale = currentScale * 1 / Math.max((b[1][0] - b[0][0]) / (width/2), (b[1][1] - b[0][1]) / (height/2));
  var nextScale = 500;
//console.log(nextScale);
   // Update the map:
  d3.selectAll(".globe path")
   .transition()
   .attrTween("d", function(d) {
      var s = d3.interpolate(currentScale, nextScale);
        return function(t) {
          projection
            //.rotate(r(Math.pow(t,0.33)))
            .scale( currentScale > nextScale ? s(Math.pow(t,0.5)) : s(Math.pow(t,3)) );
          path.projection(projection);
          return path(d);
        }
   })
   .duration(500)
   .delay(1500)
   .each("end", function() { $("#countryPopup").show(); })

}
function zoomOut(d){

	$("#countryPopup").hide();
	var currentScale = 500;
	var nextScale =  400;

   // Update the map:
  d3.selectAll(".globe path")
   .transition()
   .attrTween("d", function(d) {
      var s = d3.interpolate(currentScale, nextScale);
        return function(t) {
          projection
             .scale(s(t));
          path.projection(projection);
          return path(d);
        }
   })
   .duration(500);

}


    function country(cnt, id) { 
      for(var i = 0, l = cnt.length; i < l; i++) {
        if(cnt[i].id == id) {return cnt[i];}
      }
    };

	$( "#close" ).click(function() {
  $("#countryPopup").hide();
});

  };